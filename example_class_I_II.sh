#!/bin/bash

## Platform: Linux
##Source your conda
source <path>/conda/etc/profile.d/conda.sh
## Activate LOHHLA conda environment . You can make one using the .yml provided in the repository.
conda activate LOHHLA
## Load all the required modules
module load samtools/1.3.1
module load jellyfish/2.2.7
## Script to execute
Rscript <path>/LOHHLAscript.inhouse.CLASS_I_II_F3588_toolsPathsUpdate_hg38.R --patientId Exome --outputDir <path>/output_classII_04152020 --normalBAMfile <path>/PBL.bam --BAMDir <path>/bams_tumor_dir --hlaPath <path>/hlas_class_I_II\
--coordinateFile <path>/GRCh38_MHC_coordinates_AS.txt --HLAfastaLoc <path>/hla_ref/hla_gen.fasta --HLAexonLoc <path>/hla_ref/hla.dat --CopyNumLoc copynumber.txt --mappingStep TRUE --coverageStep TRUE\
--minCoverageFilter 10 --fishingStep TRUE --cleanUp FALSE  --gatkDir {path to executable} --novoDir {path to executable} --bedtools {path to executable} --jellyfish {path to executable} --samtools {path to executable}
